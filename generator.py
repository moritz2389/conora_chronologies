import os, shutil
from jinja2 import Template, Environment, FileSystemLoader
from loguru import logger 
import subprocess

country_list = [{"name":'Germany','code':'de'},{"name":'France','code':'fr'},{"name":'Belgium','code':'be'}, {"name":'Austria','code':'at'}, {'name':'Latvia','code':'lv'}]


class SiteGenerator(object):
    def __init__(self):
        self.feeds = []
        self.env = Environment(loader=FileSystemLoader('template'))
        self.countries=[]
        self.create_country_output()
        self.empty_public()
        self.copy_static()
        self.render_page()

    def create_country_output(self):
        """create output for each country if it fails remove the country from the list"""
        try:

            self.threshold_dict={}              # collect all html strings and use them later 
            self.history_dict={}
            file_path='backend/plot_output/'
            for country in country_list:
                code=country['code']
                os.chdir('backend')
                print(os.getcwd())
                exit_code=os.system(f'python main.py {code}')
                os.chdir('..')
                print(os.getcwd())
                if exit_code!=0:
                    continue #skip the current country if there is an error

                #main(code)
                with open(f'{file_path}/thresholds_{code}.html','r') as file:
                    threshold_div_str=file.read()

                self.threshold_dict.update({code:threshold_div_str})
                shutil.copy(f'{file_path}/history_{code}.html',f'template/history/history_{code}.html')
                shutil.copy(f'{file_path}/static/{code}_style.css',f'template/history/static/{code}_style.css')
                hist_str=f'<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="history/history_{code}.html" allowfullscreen></iframe></div>'
                self.history_dict.update({code:hist_str})
                self.countries.append(country)

        
        except Exception as e:
            logger.error(f'Error with country {code}: {e}')

    def empty_public(self):
        """ Ensure the public directory is empty before generating. """
        try:
            shutil.rmtree('./public') 
            os.mkdir('./public')
        except Exception as e:
            print("Error cleaning up old files.")

    def copy_static(self):
        """ Copy static assets to the public directory """
        try:
            #shutil.copytree('template/static', 'public/static')
            shutil.copytree('template/history', 'public/history')
        except Exception as e:
            logger.error(f"Error copying static files: {str(e)}")

    def render_page(self):
        try:
            template = self.env.get_template('minimal_bootstrap.html')
            with open('public/index.html', 'w+') as file:
                html = template.render(
                    title = "Spiffy Feeds",
                    country_list=self.countries,
                    first_country_name=self.countries[0]['name'],
                    threshold_dict=self.threshold_dict,
                    history_dict=self.history_dict
                )
                file.write(html)

        except Exception as e:
            logger.error(e)


if __name__ == "__main__":
    SiteGenerator()
